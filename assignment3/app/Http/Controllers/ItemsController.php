<?php

/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

class ItemsController extends Controller
{
    public function loadMain()
    {
        if (session('isLoggedIn') == false) {
            return redirect('assignment3/login.php');
        } else {
            return view('main', [
                'title' => 'Main',
                'books' => \App\Items::whereUserId(session('id'))->get()
            ]);
        }
    }

    public function saveItem()
    {
        $validator = \Validator::make($_POST, [
            'item' => 'required'
        ], [
            'item.required' => 'Please Enter a Book.'
        ]);

        if ($validator->fails()) {
            return redirect('/assignment3/main.php')
                ->withErrors($validator)
                ->withInput();
        } else {
            $books = new \App\Items;
            $books->name = $_POST['item'];
            $books->user_id = session('id');
            $books->save();

            return redirect('/assignment3/main.php');
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();

        return redirect('/assignment3/login.php');
    }
}
