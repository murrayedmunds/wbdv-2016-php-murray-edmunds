<?php

/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function createUsers()
    {
        return view('registration', [
            'title' => 'Registration'
        ]);
    }

    public function saveUser()
    {
        $validator = \Validator::make($_POST, [
            'name' => 'required|regex:@^[\w\-\s]+$@',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|regex:@^.*(?=.*\W)(?=.*[a-z])(?=.*[A-Z]).*$@'
        ], [
            'name.regex' => 'Name can only be Alphanumeric Characters!',
            'email.email' => 'Please Enter a valid email',
            'email.unique' => 'That email is already taken, please try another.',
            'password.regex' => 'Your password is not complex enough.'
        ]);

        if ($validator->fails()) {
            return redirect('/assignment3/registration.php')
                ->withErrors($validator)
                ->withInput();
        } else {
            $users = new \App\Users;
            $users->name = $_POST['name'];
            $users->email = $_POST['email'];
            $users->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $users->save();
            return redirect('/assignment3/login.php');
        }
    }

    public function login()
    {
        return view('login', [
            'title' => 'Login'
        ]);
    }

    public function loginValid()
    {
        $validator = \Validator::make($_POST, [
            'email' => 'required|email_exists',
            'password' => 'required|valid_password'
        ], [
            'email.required' => 'Please Enter a Email.',
            'email_exists' => 'Email entered does not exists. Please Register first.',
            'password.required' => 'Please Enter a Password.',
            'valid_password' => 'Incorrect password, try again.'
        ]);

        if ($validator->fails()) {
            return redirect('/assignment3/login')
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = \App\Users::whereEmail($_POST['email'])->first();
            $userId = $user['id'];
            $userName = $user['name'];

            session_start();
            session(['isLoggedIn' => true,
                'id' => $userId,
                'user' => $_POST['email'],
                'name' => $userName
            ]);

            return redirect('/assignment3/main.php');
        }
    }
}
