<?php

/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('assignment3/reset.php', function() {
    Artisan::call('migrate:refresh');
});

Route::get('assignment3/registration.php', 'UsersController@createUsers');
Route::post('assignment3/saveuser', 'UsersController@saveUser');

Route::get('assignment3/login.php', 'UsersController@login');
Route::post('assignment3/loginvalid', 'UsersController@loginValid');

Route::get('assignment3/main.php', 'ItemsController@loadMain');
Route::post('assignment3/saveitem', 'ItemsController@saveItem');

Route::get('assignment3/logout.php', 'ItemsController@logout');
