<!DOCTYPE html>
<html>
<!--
Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3
-->
<head>
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <hearder>
        <ul class="nav nav-pills">
            <li><a href="/assignment3/registration">Registration</a></li>
            <li><a href="/assignment3/login">Login</a></li>
            <li><a href="/assignment3/main">Main</a></li>
        </ul>
    </hearder>
    @yield('content')

<footer>
    Copyright {{ date('Y') }} Murray Edmunds
</footer>
</body>
</html>
