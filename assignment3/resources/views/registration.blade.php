<?php
/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/
?>
@extends('layouts.main')

@section('content')
    <h1>{{ $title }}</h1>
    <form method="POST" action="/assignment3/saveuser">
        {{ csrf_field() }}
        <label>Name: <input type="text" name="name" size="34" value="{{ old('name') }}"></label><br>
        <label>Email: <input type="text" name="email" size="34" value="{{ old('email') }}"></label><br>
        <label>Password: <input type="password" name="password" size="30"></label><br>
        <button type="submit">Submit</button>
    </form>
    @if ($errors->all() >0 )
        <ul class="errors list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection
