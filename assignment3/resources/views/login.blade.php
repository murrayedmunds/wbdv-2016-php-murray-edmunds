<?php
/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/
?>
@extends('layouts.main')

@section('content')
    <h1>{{ $title }}</h1>
    <form method="POST" action="/assignment3/loginvalid">
        {{ csrf_field() }}
        <input type="email" name="email" value={{ old('email') }}>
        <input type="password" name="password">
        <button type="submit">Submit</button>
    </form>
    @if ($errors->all() >0 )
        <ul class="errors list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection
