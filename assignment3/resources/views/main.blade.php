<?php
/*Author: Murray Edmunds
Date: 21 Nov 2016
Course Module: CPNT 262
Assignment: 3*/
?>
@extends('layouts.main')

@section('content')
    <a href="/assignment3/logout">sign out</a>
    <h1>{{ $title }}</h1>
    <h3>Hello, {{ session('name') }}.</h3>
    <form method="post" action="/assignment3/saveitem">
        {{ csrf_field() }}
        <label>Book Name: <input type="text" name="item" value="{{ old('item') }}"></label>
        <button type="submit" id="Submit">Enter Book</button>
    </form>
    @if ($errors->all() >0 )
        <ul class="errors list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <h3>Books</h3>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Book Title</th>
        </tr>
        @foreach ($books as $book)
            <tr>
                <td>{{ $book->id }}</td>
                <td>{{ $book->name }}</td>
            </tr>
        @endforeach
    </table>
@endsection
