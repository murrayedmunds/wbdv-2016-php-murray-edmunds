<?php
require 'src/functions.php';
require 'src/Form.php';

$form = new Form();

if ($form->wasSubmitted()) {
    if ($form->hasNoErrors()) {
        echo 'Success!';
    } else {
        echo $form->errors();
    }
}

?>

<form method="post">
    <input type="text" name="thing"/>
    <button type="submit">Submit</button>
</form>
