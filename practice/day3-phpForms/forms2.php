<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function fieldHasValue($fieldName)
{
  return !empty($_POST[$fieldName]);
}

function fieldIsTooLong($fieldName, $length)
{
  return strlen($_POST[$fieldName]) > $length;
}


?>

<form method="post">
  <?php if(formWasSubmitted() && !fieldHasValue('field_1')){ ?>
    <p>You need to enter a value.</p>
  <?php } ?>
  <?php if(formWasSubmitted() && fieldIsTooLong('field_1', 10)){ ?>
    <p>Maximum length is 10</p>
  <?php } ?>
  <input name="field_1" type="text" />
  <button type="submit">Submit</button>
</form>
