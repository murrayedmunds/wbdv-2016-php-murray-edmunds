<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function fieldHasValue($fieldName)
{
  return !empty($_POST[$fieldName]);
}

function fieldIsTooLong($fieldName, $length)
{
  return strlen($_POST[$fieldName]) > $length;
}

$options = [
  1 => 'One',
  2 => 'Two',
  3 => 'Three',
  4 => 'Four',
  5 => 'Five'
];

function select($name, $options, $extra='')
{
  $output = '';
  $output .= "<select name='$name' $extra>";
  foreach ($options as $key => $value) {
    $output .= "<option value='$key'>$value</option>";
  }
  $output .= "</select>";

  return $output;
}


?>

<form method="post">

  <?php echo select('choices', $options, 'class="form-control"'); ?>

  <?php foreach ($options as $key => $value) { ?>
    <label><?php echo $value ?></label>
    <input type="checkbox" value="on" name="<?php echo $key ?>" />
  <?php } ?>

  <?php foreach ($options as $key => $value) { ?>
    <label><?php echo $value ?></label>
    <input type="radio" name="choices" value="<?php echo $key ?>" />
  <?php } ?>

  <button type="submit">Submit</button>
</form>
