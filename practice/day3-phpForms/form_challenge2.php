<?php
	function formWasSubmitted(){
    return !empty($_GET);
  }
	function redirect($url){
		header('Location: '.$url);
	}
	if(formWasSubmitted()){
		redirect('form_challenge2_redirect.php');
	}
?>

<html>
<body>
	<form method="GET" action="form_challenge2_redirect.php">
		<input type="text" name="field_1" value="My Great Idea">
		<button type="submit">Submit</button>
	</form>
</body>
</html>