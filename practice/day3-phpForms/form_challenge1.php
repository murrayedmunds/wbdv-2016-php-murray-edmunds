<?php
  function formWasSubmitted(){
    return !empty($_POST);
  }
  function bothCheckBox($fieldName1, $fieldName2){
    if(ifchecked($fieldName1) && ifchecked($fieldName2)){
      return false;
    }else{
      return true;
    }
  }
  function ifchecked($fieldName){
    return isset($_POST[$fieldName]);
  }
?>
<html>
<body>
  <form method="post">
    <label><input type="checkbox" name="checkbox_a">A</label><br>
    <label><input type="checkbox" name="checkbox_b">B</label><br>
    <button type="submit">Submit</button>
    <?php if(formWasSubmitted() && !bothCheckBox('checkbox_a','checkbox_b')): ?>
      <p>ERROR!</p>
    <?php endif ?>
  </form>
</body>
</html>