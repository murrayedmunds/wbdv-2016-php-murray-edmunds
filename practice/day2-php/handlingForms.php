
<pre>
<?php
	
	
	function collectErrors(){
		$errors=[];
		if(!validField('tweet')){
			$errors[]=generateError('You need to enter a tweet.');
		}
		if(tweetToLong()){
			$errors[]=generateError('Your tweet is too long.');
		}
		if(!validField('name')){
			$errors[]=generateError('You need to enter a name.');
		}
		return $errors;
	}
	function showErrors($errors){
		foreach ($errors as $error) {
			echo $error;
		}
	}
	function forWasSubmitted(){
		return !empty($_GET);
	}
	function validField($fieldName){
		return !empty($_GET[$fieldName]);
	}
	function tweetToLong(){
		return strlen($_GET['tweet'])>10;
	}
	function generateError($error){
		return 'ERROR: '.$error.' ';
	}
	function showSuccessMessage(){
		foreach ($_GET as $key) {
			echo $key.' ';
		}
	}
	function redirectToSuccessPage(){
		header('Location: success.php');
	}

?>
</pre>

<html>
	<body>
		<form method="get">
			Tweet: <input type="text" name="tweet" id="tweet"/>
			Name: <input type="text" name="name" id="name"/>
			<!--
			Password: <input type="password" name="password"> -->
			<input type="submit" value="submit">
		</form>
		<?php
			if(forWasSubmitted()){
				$errors=collectErrors();
				if(!empty($errors)){
					showErrors($errors);
				}else{
					redirectToSuccessPage();
					//showSuccessMessage();
				}
			}
		?>
	</body>
</html>


