<?php

function formWasSubmitted()
{
    return !empty($_POST);
}
function fieldIsEmpty()
{
    $fields = ['field_1'];
    foreach ($fields as $field) {
        if (!empty($_POST[$field])){
            return false;
        }
    }
    return true;
    //return empty($_POST['field_1']);
}
function layoutView($viewFile, $data=[])
{
    extract($data);
    ob_start();
    require $viewFile;
    $content = ob_get_clean();

    require 'views/layouts/main.php';
}
