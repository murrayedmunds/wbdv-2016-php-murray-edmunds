<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<header>
    <h1><?php echo $title ?></h1>
    <ul class="nav nav-pills">
        <li><a href="form.php">Form</a></li>
        <li><a href="about.php">About</a></li>
    </ul>
</header>
<?php echo $content ?>
<footer>
    Copyright <?php echo date('Y')?> Murray Edmunds
</footer>
</body>
</html>