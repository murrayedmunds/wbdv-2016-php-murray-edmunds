<?php
require 'src/functions.php';
$messages=[];
if (formWasSubmitted() && fieldIsEmpty())
{
    $messages[]='Field is Empty';
}
layoutView('views/form.php', [
    'messages' => $messages,
    'title' => 'Form'
]);
