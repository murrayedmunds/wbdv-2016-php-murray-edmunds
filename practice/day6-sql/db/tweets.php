<?php

function formWasSubmitted()
{
    return !empty($_POST);
}

function getTweets()
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets');

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function getTweetsForUserId($userId){
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets WHERE user_id = '.$userId);

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function saveTweet()
{
    date_default_timezone_set('America/Edmonton');
    $tweetText = $_POST['text'];
    $user_id = $_POST['user_id'];
    $currentDateTime = date('Y-m-d H:i:s');

    $db = new mysqli('localhost', 'sait', '1234', 'twitter');
    $tweetText = $db->real_escape_string($tweetText);
    $user_id = $db->real_escape_string($user_id);

    $sql = "INSERT INTO `twitter`.`tweets` (`id`, `text`, `datetime`, `user_id`) VALUES (NULL, '$tweetText', '$currentDateTime', '$user_id');";
    $result = $db->query($sql);

}
if (formWasSubmitted()) {
    saveTweet();
}

$tweets = getTweetsforUserId(2);

?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
</head>

<form method="post">
    <input type="text" name="text" placeholder="tweet"/>
    <input type="number" name="user_id" placeholder="number"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<table class="table">
<?php foreach ($tweets as $tweet) { ?>
    <tr>
    <td><?= $tweet['text'] ?></td>
    <td><?= $tweet['dateTime'] ?></td>
    </tr>
<?php } ?>
</table>
