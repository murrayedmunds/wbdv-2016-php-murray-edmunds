<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<pre>
<?php

$db= new mysqli('localhost','sait','1234','twitter');

$results = $db->query('SELECT * from users');
$users = $results->fetch_all(MYSQLI_ASSOC);

function formWasSubmitted(){
    return !empty($_POST);
}

function createUser(){
    date_default_timezone_set ( 'America/Edmonton' );
    $userHandle = $_POST['handle'];
    $userEmail = $_POST['email'];
    $userPassword = $_POST['password'];
    $userRealName = $_POST['realName'];

    $sql = "INSERT INTO `twitter`.`users` (`id`, `handle`, `email`, `password`, `realName`, `isDeleted`) VALUES (NULL, '$userHandle', '$userEmail', '$userPassword', '$userRealName', '0')";

    execute($sql);
}

function getUsers(){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query('SELECT * from users');
    $users = $results->fetch_all();
    return $users;
}

function execute($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query($sql);
    if ($results == false){
        var_dump('CODE FAIL!', $db->error);
        die;
    }
}

function select($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $result = $db->query($sql);
    return $result;
}

function fieldIsUnique($fieldName){
    $fieldvalue = $_POST['fieldName'];
    $sql = "SELECT * from users where $fieldName = $fieldvalue";
    $result = select($sql);
    return $result->num_rows == 0;
}

if (formWasSubmitted()) {
    if(fieldIsUnique('users', 'email') && fieldIsUnique('users', 'handle')) {
        createUser();
    } else {
        echo 'That handle and/or email is already taken, please choose another one.';
    }
}

$user = getUsers();
?>
<body>
    <h3>Add User</h3>
    <form method="POST">
        <input type="text" name="handle" size="40" placeholder="handle"/>
        <input type="text" name="email" size="40" placeholder="email address"/>
        <input type="text" name="password" size="40" placeholder="password"/>
        <input type="text" name="realName" size="40" placeholder="realName"/>
        <button class="btn btn-default" type="submit">Add User</button>
    </form>
    <table class="table">
        <tr>
            <th>User ID</th>
            <th>Handle</th>
            <th>Email</th>
            <th>Real Name</th>
            <th>Password</th>
        </tr>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['handle'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['realName'] ?></td>
                <td><?= $user['password'] ?></td>
            </tr>
        <?php } ?>
    </table>

</body>