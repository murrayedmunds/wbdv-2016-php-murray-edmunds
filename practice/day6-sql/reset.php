<?php

function execute($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query($sql);
    if ($results == false){
        var_dump('CODE FAIL!', $db->error);
        die;
    }
}

execute('DROP TABLE IF EXISTS users');
execute('DROP TABLE IF EXISTS tweets');

$createUsers = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `handle` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `realName` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;";

$addUserKeys = "ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `handle` (`handle`),
  ADD UNIQUE KEY `email` (`email`),
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;";

$createTweets = "CREATE TABLE IF NOT EXISTS `tweets` (
  `id` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  `dateTime` datetime NOT NULL,
  `user_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;";

$addTweetKeys = "ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;";

execute($createUsers);
execute($addUserKeys);
execute($createTweets);
execute($addTweetKeys);

header('Location: users.php');
