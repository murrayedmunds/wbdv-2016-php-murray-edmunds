<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<pre>
<?php

$db= new mysqli('localhost','sait','1234','twitter');

$userResults = $db->query('SELECT * from users');
$tweetResults = $db->query('SELECT * from tweets');
$users = $userResults->fetch_all(MYSQLI_ASSOC);
$tweets = $tweetResults->fetch_all(MYSQLI_ASSOC);

//var_dump($userRows);

/*function createTable($table){
    echo '<table class="table table-striped table-bordered ">';
    foreach ($table as $row) {
        echo '<tr>';
        for($i=0;$i<count($row);$i++){
            echo '<td>' . $row[$i] . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}*/

function formWasSubmitted(){
    return !empty($_POST);
}

function saveTweet(){
    date_default_timezone_set ( 'America/Edmonton' );
    $tweetText = $_POST['text'];
    $user_id = $_POST['user_id'];
    $currentDate = date('Y-m-d H:i:s');
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $tweetText = $db->real_escape_string($tweetText);
    $user_id = $db->real_escape_string($user_id);

    $sql = "INSERT INTO `twitter`.`tweets` (`id`, `text`, `dateTime`, `user_id`) VALUES (NULL, '$tweetText', '$currentDate', '$user_id')";
    execute($sql);
}

function getTweetsForUserId($userId)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets WHERE user_id = '.$userId);

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function getTweets()
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets');

    $tweets = $result->fetch_all();

    return $tweets;
}

function userNameForTweetId($tweetId){
    $sql = 'SELECT * from users WHERE id = '. $tweetId;
    //var_dump($sql);
    //die;
    $results = select($sql);
    $user = $results->fetch_assoc();

    $userName = $user['realName'];
    return $userName;
}

function createUser(){
    date_default_timezone_set ( 'America/Edmonton' );
    $userHandle = $_POST['handle'];
    $userEmail = $_POST['email'];
    $userPassword = $_POST['password'];
    $userRealName = $_POST['realName'];

    $sql = "INSERT INTO `twitter`.`users` (`id`, `handle`, `email`, `password`, `realName`, `isDeleted`) VALUES (NULL, '$userHandle', '$userEmail', '$userPassword', '$userRealName', '0')";

    execute($sql);
}

function getUsers(){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query('SELECT * from users');
    $users = $results->fetch_all();
    return $users;
}

function execute($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query($sql);
    if ($results == false){
        var_dump('VERY BAD ERROR', $db->error);
        die;
    }
}

function select($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $result = $db->query($sql);
    return $result;
}

function fieldIsUnique($fieldName){
    $fieldvalue = $_POST['fieldName'];
    $sql = "SELECT * from users where $fieldName = $fieldvalue";
    $result = select($sql);
    return $result->num_rows == 0;
}

if (formWasSubmitted()) {
    if (!empty($_POST['text'])){
        saveTweet();
    }
    if (!empty($_POST['handle'])){
        if(fieldIsUnique('users', 'email') && fieldIsUnique('users', 'handle')) {
            createUser();
        } else {
            echo 'That handle and/or email is already taken, please choose another one.';
        }
    }
}

//$tweetRows = getTweets();
$tweets = getTweetsForUserId(2);
$userRows = getUsers();
?>
<body style="">
    <div style="float: left; width: 40%;">
        <h3>Enter a Tweet</h3>
        <form method="POST">
            <input type="text" name="text" placeholder="tweet"/>
            <input type="number" name="user_id" placeholder="user id"/>
            <button class="btn btn-default" type="submit">Enter Tweet</button>
        </form>
        <table class="table">
            <tr>
                <th>Tweet</th>
                <th>Date & Time</th>
            </tr>
        <?php foreach ($tweets as $tweet) { ?>
            <tr>
                <td><?= $tweet['text'] ?></td>
                <td><?= $tweet['dateTime'] ?></td>
                <td><?= userNameForTweetId($tweet['user_id']) ?></td>
            </tr>
        <?php } ?>
        </table>
    </div>
    <div style="float: right; width: 60%;">
        <h3>Add User</h3>
        <form method="POST">
            <input type="text" name="handle" placeholder="handle"/>
            <input type="text" name="email" placeholder="email address"/>
            <input type="text" name="password" placeholder="password"/>
            <input type="text" name="realName" placeholder="realName"/>
            <button class="btn btn-default" type="submit">Add User</button>
        </form>
        <table class="table">
            <tr>
                <th>User ID</th>
                <th>Handle</th>
                <th>Email</th>
                <th>Real Name</th>
                <th>Password</th>
            </tr>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['handle'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['realName'] ?></td>
                <td><?= $user['password'] ?></td>
            </tr>
        <?php } ?>
        </table>
    </div>
</body>