<?php

function userIsNotLoggedIn()
{
    session_start();

    if (isset($_SESSION['isLoggedIn']))
    {
        return false;
    } else {
        return true;
    }
}

if (userIsNotLoggedIn())
{
    header('Location: login.php');
}
?>

<a href="logout.php">sign out</a>

<h1>Hello <?= $_SESSION['user']['realName'] ?></h1>
