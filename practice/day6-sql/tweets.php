<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<pre>
<?php

$db= new mysqli('localhost','sait','1234','twitter');

$results = $db->query('SELECT * from tweets');
$tweets = $results->fetch_all(MYSQLI_ASSOC);

function formWasSubmitted(){
    return !empty($_POST);
}

function saveTweet(){
    date_default_timezone_set ( 'America/Edmonton' );
    $tweetText = $_POST['text'];
    $user_id = $_POST['user_id'];
    $currentDate = date('Y-m-d H:i:s');
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $tweetText = $db->real_escape_string($tweetText);
    $user_id = $db->real_escape_string($user_id);

    $sql = "INSERT INTO `twitter`.`tweets` (`id`, `text`, `dateTime`, `user_id`) VALUES (NULL, '$tweetText', '$currentDate', '$user_id')";
    execute($sql);
}

function getTweetsForUserId($userId)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets WHERE user_id = '.$userId);

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function getTweets()
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets');

    $tweets = $result->fetch_all();

    return $tweets;
}

function realNameForUserId($tweetId){
    $sql = 'SELECT * from users WHERE id = '. $tweetId;

    $results = select($sql);
    $user = $results->fetch_assoc();

    $userName = $user['realName'];
    return $userName;
}


function execute($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $results = $db->query($sql);
    if ($results == false){
        var_dump('CODE FAIL!', $db->error);
        die;
    }
}

function select($sql){
    $db= new mysqli('localhost','sait','1234','twitter');
    $result = $db->query($sql);
    return $result;
}


if (formWasSubmitted()) {
    saveTweet();
}

//$tweetRows = getTweets();
$tweets = getTweetsForUserId(1);
?>
<body>
    <h3>Enter a Tweet</h3>
    <form method="POST">
        <input type="text" name="text" size="30" placeholder="tweet"/>
        <input type="number" name="user_id" placeholder="user id"/>
        <button class="btn btn-default" type="submit">Enter Tweet</button>
    </form>
    <table class="table">
        <tr>
            <th>Tweet</th>
            <th>Date & Time</th>
            <th>User Name</th>
        </tr>
        <?php foreach ($tweets as $tweet) { ?>
            <tr>
                <td><?= $tweet['text'] ?></td>
                <td><?= $tweet['dateTime'] ?></td>
                <td><?= realNameForUserId($tweet['user_id']) ?></td>
                <td><a href="deletetweet.php?id=<?= $tweet['id'] ?>">delete</a></td>
            </tr>
        <?php } ?>
    </table>

</body>