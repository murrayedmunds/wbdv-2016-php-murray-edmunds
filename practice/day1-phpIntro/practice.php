<?php
echo 'Hello World'.'<br>';
$firstName='Murray';
echo $firstName.'<br>';
echo 'My name'.' is '.'<br>';
$lastName='Edmunds';
echo $firstName.' '.$lastName.'<br>';
$num1=12;
$num2=21;
echo $num1+$num2.'<br>';
echo $num1-$num2.'<br>';
echo $num1*$num2.'<br>';
echo $num1/$num2.'<br>';
if($num1>$num2){
	echo 'Number 1 is greater than Number 2'.'<br>';
}else{
	echo 'Number 1 is less than Number 2'.'<br>';
};

$dogNames=['Foxy','Fido','Sam','Gus','Tanya'];
foreach ($dogNames as $name) {
	echo $name.'<br>';
}

$contact=[
	'name' => 'Murray Edmunds',
	'email' => 'murray.edmunds@live.com',
	'city' => 'Calgary'
];
foreach ($contact as $index => $output) {
	echo $index.': '.$output.' ';
}
echo '<br>';
echo join(',',$dogNames);
echo '<br>';
array_push($dogNames, 'Thor');
foreach ($dogNames as $name) {
	echo $name.'<br>';
}
$numArray=[0,5,9,97,-40,32.2];
foreach ($numArray as $num) {
	echo $num.' ';
}
echo '<br>';
sort($numArray);
foreach ($numArray as $num) {
	echo $num.' ';
}
echo '<br>';
function printHello(){
	echo 'Hello World!';
}
printHello();
echo '<br>';
function myName($name){
	return 'Hello my name is '.$name;
}
$murray=myName('Murray');
echo $murray;
echo '<br>';
class Creature{
	public $numLegs;
	public function describe(){
		echo 'I have '.$this->numLegs.' legs!';
	}
}
class Spider extends Creature{
	public $numLegs=8;
}
class Blob extends Creature{
	public $numLegs=0;
}
$charolette=new Spider();
$charolette->describe();
echo '<br>';
$amazing=new Blob();
$amazing->describe();



?>