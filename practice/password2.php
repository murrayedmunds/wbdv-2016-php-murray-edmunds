<?php
    $realPassword = password_hash('herman', PASSWORD_DEFAULT);

    function formWasSubmitted()
    {
        return !empty($_POST);
    }

    function passwordIsCorrect($enteredPassword, $realPassword)
    {
        return password_verify($enteredPassword, $realPassword);
    }



    if (formWasSubmitted())
    {
        if(passwordIsCorrect($_POST['password'], $realPassword))
        {
            echo 'You Are Correct!';
        } else {
         echo 'You Shall Not PASS!';
        }
    }
?>

<form method="post">
    <input type="password" name="password">
    <button type="submit">Submit</button>
</form>
