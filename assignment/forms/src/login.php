<!--
Author: Murray Edmunds
Date: 07 Nov 2016
Course Module: CPNT 262
Assignment: 1
-->
<?php
function layoutView($viewFile, $data)
{
    extract($data); //parse $data array to strings

    ob_start(); //start recording
    require $viewFile; //record this file
    $content = ob_get_clean(); // save recoding to $content

    require 'views/layouts/main.php'; //add main content to page use recorded content in body
}
function formWasSubmitted()
{
    return !empty($_POST);
}
function collectAllErrors()
{
    $errors = [];
    if (formWasSubmitted() && incorrectPassword('email', 'password')) {
        $errors[] = 'ERROR! Incorrect password.';
    }
    if (formWasSubmitted() && incorrectEmail('email', 'password')) {
        $errors[] = 'ERROR! Incorrect email."';
    }
    return $errors;
}
function showErrors($errors)
{
    foreach ($errors as $error) {
        echo $error;
    }
}
function incorrectPassword($email, $password)
{
    if (checkEmail($_POST[$email]) && !checkPassword($_POST[$password])) {
        return true;
    } else {
        return false;
    }
}
function incorrectEmail($email, $password)
{
    if (!checkEmail($_POST[$email])) {
        return true;
    } else {
        return false;
    }
}
function checkEmail($email)
{
    return $email=='correct@email.com';
}
function checkPassword($password)
{
    return $password=='correct-password';
}
function redirectToNextPage($url)
{
    header('Location: '.$url);
}
