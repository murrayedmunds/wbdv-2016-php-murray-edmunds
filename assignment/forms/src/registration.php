<!--
Author: Murray Edmunds
Date: 07 Nov 2016
Course Module: CPNT 262
Assignment: 1
-->
<?php
function layoutView($viewFile, $data)
{
    extract($data);

    ob_start();
    require $viewFile;
    $content = ob_get_clean();

    require 'views/layouts/main.php';
}
function formWasSubmitted()
{
    return !empty($_POST);
}
function redirectToNextPage($url)
{
    header('Location: '.$url);
}
