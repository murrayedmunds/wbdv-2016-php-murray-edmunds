<!--
Author: Murray Edmunds
Date: 07 Nov 2016
Course Module: CPNT 262
Assignment: 1
-->
<?php
require 'src/registration.php';

if (formWasSubmitted()) {
    redirectToNextPage('login.php');
}

// call function layoutView and pass it locations of its views file and title
layoutView('views/registration.php', [
    'title' => 'Registration'
]);
