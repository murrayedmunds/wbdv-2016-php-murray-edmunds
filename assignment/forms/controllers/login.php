<!--
Author: Murray Edmunds
Date: 07 Nov 2016
Course Module: CPNT 262
Assignment: 1
-->
<?php
require 'src/login.php'; //get the functions

// set variables to be able to use outside of functions
$errors=[];
$emailValue=''; // when page loads set value of email to ''

if (formWasSubmitted()) { //check if from was submitted
    $errors = collectAllErrors(); //collect errors from collectAllErrors function
    if (empty($errors)) { // after sumbit was pressed and no errors detected then
        redirectToNextPage('main.php'); // redirect
        // showSuccessMessage();
    }
}

if (formWasSubmitted() && isset($_POST['email'])) { //check that form was submitted and email field has a value
    $emailValue=$_POST['email']; // set emailValue equal to value of email field
}

// call function layoutView and pass it locations of its views file, errors collected, email field value and title
layoutView('views/login.php', [
    'errors' => $errors,
    'emailValue' => $emailValue,
    'title' => 'Login'
]);
