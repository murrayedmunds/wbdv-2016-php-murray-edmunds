<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

function createUser()
{
    $db= new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');
    $name = $db->real_escape_string($_POST['name']);
    $email = $db->real_escape_string($_POST['email']);
    $password = $db->real_escape_string($_POST['password']);
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    $sql = "INSERT INTO `wbdv2016`.`users` (`id`, `name`, `email`, `password`) VALUES (NULL, '$name', '$email', '$hashedPassword')";
    execute($sql);
}

function getUserInfo($email, $element)
{
    $sql = "SELECT * FROM users WHERE email = '".$email."'";
    $results = select($sql);
    $user = $results->fetch_assoc();
    return $user[$element];
}

function passwordVerified($email, $password)
{
    $sql = "SELECT * FROM users WHERE email = '".$email."'";
    $results = select($sql);
    $user = $results->fetch_assoc();
    $userPassword = $user['password'];
    //verify password entered on login page matches database password for that email
    return password_verify($password, $userPassword);
}

function saveBook()
{
    $book = $_POST['item'];
    $user_id = $_SESSION['id'];
    $db= new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');

    $book = $db->real_escape_string($book);
    $user_id = $db->real_escape_string($user_id);

    $sql = "INSERT INTO `wbdv2016`.`items` (`id`, `user_id`, `name`) VALUES (NULL, '$user_id', '$book')";
    execute($sql);
}

function getBooks($userId)
{
    $db= new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');

    $result = $db->query('SELECT * from items WHERE user_id = '.$userId);

    $books = $result->fetch_all(MYSQLI_ASSOC);

    return $books;
}

function execute($sql)
{
    $db= new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');
    $results = $db->query($sql);
    if ($results == false) {
        var_dump('CODE FAIL!', $db->error);
        die;
    }
}

function select($sql)
{
    $db= new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');
    $result = $db->query($sql);
    return $result;
}

function fieldIsUnique($table, $fieldName, $sessionId = '')
{
    $fieldValue = $_POST[$fieldName];
    if ($table == 'users') {
        $sql = "SELECT * from ".$table." where ".$fieldName." = '".$fieldValue."'";
    } else if ($table == 'items') {
        $fieldName = 'name';
        $sql = "SELECT * from ".$table." where ".$fieldName." = '".$fieldValue."' AND user_id = ".$sessionId;
    }
    $result = select($sql);
    return $result->num_rows == 0;
}
