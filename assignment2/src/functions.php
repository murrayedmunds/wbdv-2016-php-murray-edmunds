<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

function layoutView($viewFile, $data)
{
    //parse $data array to strings
    extract($data);

    //start recording
    ob_start();

    //record this file
    require $viewFile;

    // save recoding to $content
    $content = ob_get_clean();

    //add main content to page use recorded content in body
    require 'views/layouts/main.php';
}

function formWasSubmitted()
{
    return !empty($_POST);
}

function collectAllErrors($page = '')
{
    require 'src/strings.php';
    $errors = [];
    // error checks for registration page only
    if ($page == 'registration') {
        //check that a name field is entered
        if (!fieldEntered('name')) {
            $errors[] = 'Please Enter a Name';
        }
        //check that email field is entered
        if (!fieldEntered('email')) {
            $errors[] = 'Please Enter a Email';
        }
        //check that password field is entered
        if (!fieldEntered('password')) {
            $errors[] = 'Please Enter a Password';
        }
        //check that name only contains alphanumeric characters
        if (regexCheck($_POST['name'], $nameRegex)) {
            $errors[] = 'Name can only be Alphanumeric Characters';
        }
        //check that email is proper format
        if (regexCheck($_POST['email'], $emailRegex)) {
            $errors[] = 'Please Enter a valid email';
        }
        //check that password contains lowercase, uppercase, and a non alphanumeric character
        if (regexCheck($_POST['password'], $passwordRegex)) {
            $errors[] = 'Your password is not complex enough.';
        }
        //check that name entered isn't already in database
        /*if (!fieldIsUnique('users', 'name')) {
            $errors[] = 'That name is already taken, please try another.';
        }*/
        if (!fieldIsUnique('users', 'email')) {
            $errors[] = 'That email is already taken, please try another.';
        }
    }
    //error checks for login page only
    if ($page == 'login') {
        //check that email field is entered
        if (!fieldEntered('email')) {
            $errors[] = 'Please Enter a Email';
        }
        //check that password field is entered
        if (!fieldEntered('password')) {
            $errors[] = 'Please Enter a Password';
        }
        //check that entered email is in the database
        if (!getUserInfo($_POST['email'], 'email')) {
            $errors[] = 'Email entered does not exists. Please Register first.';
            // check that password entered matches registered password for the email entered
        } else if (!passwordVerified($_POST['email'], $_POST['password'])) {
            $errors[] = 'Incorrect password, try again.';
        }
    }
    //error checks for main page only
    if ($page == 'main') {
        //check that book field is entered
        if (!fieldEntered('item')) {
            $errors[] = 'Please Enter a book';
        }
        //check that the user hasn't already entered that book
        if (!fieldIsUnique('items', 'item', $_SESSION['id'])) {
            $errors[] = "You've already entered that book";
        }
    }
    return $errors;
}

function showErrors($errors)
{
    foreach ($errors as $error) {
        echo $error;
    }
}

function redirect($url)
{
    header('Location: '.$url);
}

function fieldEntered($fieldName)
{
    return !empty($_POST[$fieldName]);
}

function regexCheck($input, $pattern)
{
    require 'src/strings.php';
    return !preg_match($pattern, $input);
}

function userIsNotLoggedIn()
{
    session_start();

    if (isset($_SESSION['isLoggedIn'])) {
        return false;
    } else {
        return true;
    }
}

function loginUser($email)
{
    session_start();
    $_SESSION['isLoggedIn']='true';
    $_SESSION['id'] = getUserInfo($email, 'id');
    $_SESSION['user'] = $email;
    $_SESSION['name'] = getUserInfo($email, 'name');
    var_dump($_SESSION);
    redirect('main.php');
}
