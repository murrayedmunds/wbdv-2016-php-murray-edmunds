<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

$emailRegex = '/^(\w+\.)*\w+@([A-z]+\.)+[A-z]{2,6}$/';
$nameRegex = '|^[\w\-\s]+$|';
$passwordRegex = '|^.*(?=.*\W)(?=.*[a-z])(?=.*[A-Z]).*$|';
