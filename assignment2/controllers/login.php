<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

//get the functions
require 'src/functions.php';
require 'src/database.php';

// set variables to be able to use outside of functions
$errors=[];
// when page loads set value of email to ''
$emailValue='';

//check if from was submitted
if (formWasSubmitted()) {
    //collect errors from collectAllErrors function
    $errors = collectAllErrors('login');
    // after sumbit was pressed and no errors detected then
    if (empty($errors)) {
        loginUser($_POST['email']);
    }
}

//check that form was submitted and email field has a value
if (formWasSubmitted() && isset($_POST['email'])) {
    // set emailValue equal to value of email field
    $emailValue=$_POST['email'];
}

// call function layoutView and pass it locations of its views file, errors collected, email field value and title
layoutView('views/login.php', [
    'errors' => $errors,
    'emailValue' => $emailValue,
    'title' => 'Login'
]);
