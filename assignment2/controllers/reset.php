<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

require 'src/functions.php';
require 'src/database.php';

execute('DROP TABLE IF EXISTS users');
execute('DROP TABLE IF EXISTS items');

$createUsers = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$addUserKeys = "ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
/*ADD UNIQUE KEY `username` (`name`),*/

$createItems = "CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$addItemKeys = "ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";

execute($createUsers);
execute($addUserKeys);
execute($createItems);
execute($addItemKeys);

redirect('registration.php');
