<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php

require 'src/functions.php';

session_start();

session_unset();

session_destroy();

redirect('login.php');
