<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php
//get the functions
require 'src/functions.php';
require 'src/database.php';

if (userIsNotLoggedIn()) {
    header('Location: login.php');
}

$books = getBooks($_SESSION['id']);
$name = $_SESSION['name'];
$errors=[];

if (formWasSubmitted()) {
    $errors = collectAllErrors('main');
    if (empty($errors)) {
        savebook();
        $books = getBooks($_SESSION['id']);
    }
}
// call function layoutView and pass it locations of its views file and title
layoutView('views/main.php', [
    'title' => 'Main',
    'name' => $name,
    'books' => $books,
    'errors' => $errors
]);
