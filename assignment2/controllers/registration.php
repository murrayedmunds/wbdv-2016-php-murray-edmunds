<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<?php
//get the functions
require 'src/functions.php';
require 'src/database.php';

$errors = [];

if (formWasSubmitted()) {
    $errors = collectAllErrors('registration');
    if (empty($errors)) {
        createUser();
        redirect('login.php');
    }
}

// call function layoutView and pass it locations of its views file and title
layoutView('views/registration.php', [
    'title' => 'Registration',
    'errors' => $errors
]);
