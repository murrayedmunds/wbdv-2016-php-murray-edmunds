<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<body>
    <h1><?php echo $title ?></h1>
    <form method="POST">
        <label>Name: <input type="text" name="name" size="34"></label><br>
        <label>Email: <input type="text" name="email" size="34"></label><br>
        <label>Password: <input type="password" name="password" size="30"></label><br>
        <button type="submit">Submit</button>
    </form>
    <p><?php echo htmlentities(join(', ', $errors)) ?></p>
</body>
</html>
