<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<body>
    <a href="logout.php">sign out</a>
    <h1><?php echo $title ?></h1>
    <h3>Hello, <?php echo htmlentities($name) ?>.</h3>
    <form method="post" >
        <label>Book Name: <input type="text" name="item"></label>
        <button type="submit" id="Submit">Enter Book</button>
    </form>
    <p><?php echo join(',', $errors) ?></p>
    <h3>Books</h3>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Book Title</th>
        </tr>
        <?php foreach ($books as $book) { ?>
            <tr>
                <td><?= htmlentities($book['id']) ?></td>
                <td><?= htmlentities($book['name']) ?></td>
            </tr>
        <?php } ?>
    </table>
</body>
</html>
