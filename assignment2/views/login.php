<!--
Author: Murray Edmunds
Date: 14 Nov 2016
Course Module: CPNT 262
Assignment: 2
-->
<h1><?php echo $title ?></h1>
<form method="POST">
    <input type="email" name="email" value=<?php echo $emailValue?>>
    <input type="password" name="password">
    <button type="submit">Submit</button>
</form>

<p><?php echo join(',', $errors) ?></p>
