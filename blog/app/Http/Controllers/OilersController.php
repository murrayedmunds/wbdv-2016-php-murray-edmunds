<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OilersController extends Controller
{
    public function index()
    {
        $faker = \Faker\Factory::create();

        return view('oilers.index', [
            'oilers' => \App\Oilers::all(),
            'faker' => $faker->phoneNumber,
            'fakerColor1' => $faker->hexcolor,
            'fakerColor2' => $faker->hexcolor
        ]);
    }

    public function store()
    {
        $validator = \Validator::make($_POST, [
            'name' => 'required',
            'position' => 'required',
            'number' => 'required|integer|between:1,99'

        ], [
            'name.required' => 'You must enter a Player Name.',
            'position.required' => 'Please Select a Postion for Player',
            'number.required' => 'You must enter the Players Number',
            'number.between' => 'Player Number must between 0 and 100'
        ]);

        if ($validator->fails()) {
            return redirect('/oilers/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $oiler = new \App\Oilers;
            $oiler->name = $_POST['name'];
            $oiler->position = $_POST['position'];
            $oiler->number = $_POST['number'];
            $oiler->save();
            return redirect('/oilers');
        }
    }

    public function create()
    {
        return view('oilers.create');
    }

    public function editPlayer()
    {
        return view('oilers.edit', [
            'oilers' => \App\Oilers::all()
        ]);
    }
    public function editDatabase($column, $field)
    {
        if (!empty($field)) {
            return DB::table('oilers')
                ->where('name', $_POST['nameCurrent'])
                ->update([
                    $column => $field
                ]);
        }
    }
    public function editPlayerStore()
    {
        $validator = \Validator::make($_POST, [
            'nameCurrent' => 'required',
        ], [
            'nameCurrent.required' => 'You must select a player to edit',
        ]);



        if ($validator->fails()) {
            return redirect('/oilers/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->editDatabase('name', $_POST['name']);
            $this->editDatabase('position', $_POST['position']);
            $this->editDatabase('number', $_POST['number']);
            return redirect('/oilers');
        }
    }

}
