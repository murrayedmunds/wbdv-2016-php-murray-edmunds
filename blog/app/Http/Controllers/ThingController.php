<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ThingController extends Controller
{
    public function index()
    {
        return view('things.index', [
            'things' => \App\Thing::all()
        ]);
    }

    public function store()
    {
        $validator = \Validator::make($_POST, [
            'thing' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/things/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $thing = new \App\Thing;
            $thing->name = $_POST['thing'];
            $thing->save();
            return redirect('/things');
        }
    }

    public function create()
    {
        return view('things.create');
    }
}
