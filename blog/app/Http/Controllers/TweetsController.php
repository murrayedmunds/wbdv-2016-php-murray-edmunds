<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TweetsController extends Controller
{
    public function index()
    {
        //for similar to fetchall
        $tweets = \App\Tweet::whereUserId(1)->get();
        //or
        //$tweets = \App\Tweet::where('user_id', 1)->get();
        //for similar to fetchasscc
        $user = \App\User::whereEmail('example@email.com')->first();
        return view('index', [
            'tweets' => $tweets
        ]);
    }

    public function store()
    {
        $validator = \Validator::make($_POST, [
            'tweet' => 'required',
        ], [
            'tweet.email' => 'Your tweet is not a email!',
        ]);

        if ($validator->fails()) {
            return redirect('/tweets/create')->withErrors($validator)->withInput();
        } else {
            $tweet = new \App\Tweet;
            $tweet->tweet = $_POST['tweet'];
            $tweet->user_id = 1;
            $tweet->save();
            return redirect('/tweets');
        }
    }
    public function create()
    {
        return view('create');
    }
}
