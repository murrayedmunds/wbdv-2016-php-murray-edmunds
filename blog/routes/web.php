<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/tweets/create', 'TweetsController@create');
// Route::post('/tweets/create', 'TweetsController@store');
Route::post('/tweets/store', 'TweetsController@store');
Route::get('/tweets', 'TweetsController@index');

Route::get('/things', 'ThingController@index');
Route::get('/things/create', 'ThingController@create');
Route::post('/things/store', 'ThingController@store');


Route::get('/oilers', 'OilersController@index');
Route::post('/oilers/store', 'OilersController@store');
Route::get('/oilers/create', 'OilersController@create');
Route::get('/oilers/edit', 'OilersController@editPlayer');
Route::post('/oilers/editStore', 'OilersController@editPlayerStore');

Route::get('/users', function () {
    return view('users');
});
