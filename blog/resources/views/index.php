<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
</head>
<body>
    <a href="/tweets/create/">Create A New Tweet</a>
    <table class="table table-bordered">
        <tr>
            <th>Tweet Text</th>
            <th>User ID</th>
            <th>Created @</th>
        </tr>
        <?php foreach ($tweets as $tweet) { ?>
            <tr>
                <td><?= $tweet->tweet ?></td>
                <td><?= $tweet['user_id'] ?></td>
                <td><?= $tweet['created_at'] ?></td>
            </tr>
        <?php } ?>
    </table>
</body>