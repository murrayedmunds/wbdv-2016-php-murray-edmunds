<!DOCTYPE html>
<html>
        <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
        </head>
        <body>
                <header>
                        <ul class="nav nav-pills">
                                <li><a href="/oilers">Oilers</a></li>
                                <li><a href="/oilers/create">Create Player</a></li>
                                <li><a href="/oilers/edit">Edit Players</a></li>
                        </ul>
                </header>
                @yield('content')
                <footer>Copyright {{ date('Y') }} Murray Edmunds</footer>
        </body>
</html>