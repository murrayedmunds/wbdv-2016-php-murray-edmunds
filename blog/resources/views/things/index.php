<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
</head>
<body>
<a href="/things/create/">Create A New Thing</a>
<table class="table table-bordered">
    <tr>
        <th>Things</th>
        <th>Created @</th>
    </tr>
    <?php foreach ($things as $thing) { ?>
        <tr>
            <td><?= $thing->name ?></td>
            <td><?php echo $thing->created_at ?></td>
        </tr>
    <?php } ?>
</table>
</body>