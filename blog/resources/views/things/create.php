<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
    <!--<meta name="csrf-token" content="{{ csrf_token() }}" />-->

</head>

<body>
<form method="post" action="/things/store">
    <!--<input type="hidden" name="_token" value="<?= csrf_token() ?>">-->
    <?= csrf_field() ?>
    <label>Text: <input type="text" name="thing"/></label>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php if ($errors->all() >0 ): ?>
    <?php echo join(',', $errors->all()) ?>
<?php endif; ?>
</body>