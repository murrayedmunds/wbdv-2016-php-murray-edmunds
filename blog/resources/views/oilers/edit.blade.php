@extends('layouts.main')

@section('content')
    <form method="post" action="/oilers/editStore">
        {{ csrf_field() }}
        <select name="nameCurrent">
            <option value="">Select Player</option>
        @foreach ($oilers as $oiler)
            <option value="{{$oiler->name}}">{{$oiler->name}}</option>
        @endforeach
        </select>
        <label>New Name: <input type="text" name="name"/></label>
        <select name="position">
            <option value="">Select Position</option>
            <option value="C">C</option>
            <option value="LW">LW</option>
            <option value="RW">RW</option>
            <option value="D">D</option>
            <option value="G">G</option>
        </select>
        <label>Number: <input type="number" name="number" style="width: 3em;"/></label>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @if ($errors->all() >0 )
        <ul class="errors list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection