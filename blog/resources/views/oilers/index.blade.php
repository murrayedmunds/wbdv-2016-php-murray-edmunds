@extends('layouts.main')

@section('content')
    <h1>Edmonton Oilers</h1>
    <p>{{$faker}}</p>
    <table class="table table-bordered" style="background-color: {{$fakerColor1}}; color: {{$fakerColor2}}">
        <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Number</th>
        </tr>
        @foreach ($oilers as $oiler)
            <tr>
                <td>{{ $oiler->name }}</td>
                <td>{{ $oiler->position }}</td>
                <td>{{ $oiler->number }}</td>
            </tr>
        @endforeach
    </table>
@endsection
